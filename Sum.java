package week1;

import java.util.Scanner;

class Suma {
    //написать метод который вычисляет сумму двух чисел и возвращает ее.
    public static void main( String[] args ) {
        int a, b, c = 0;
        Scanner s = new Scanner( System.in );
        System.out.println( "Введите число: ");
        a = s.nextInt();
        b = s.nextInt();
        c = a + b;
        System.out.println( "Сумма a и b равна: " + c );
    }

}
